module.exports = {
  apps: [
    {
      name: 'nuxt.project.template',
      script:
        'rm -f nuxt.socket && npm run build &>> /var/log/nuxt.project.template.log && npm run start &>> /var/log/nuxt.project.template.log',
      cwd: '/path/to/project/directory',
      autorestart: true,
      watch: false,
      max_memory_restart: '512M',
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
}
